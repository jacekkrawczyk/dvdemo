unit DVDemo.Types;

interface

uses
  // Delphi Library Units
  System.Generics.Collections,
  System.SysUtils;

type
  EDVDemo = class(Exception);

  TDVDemoItem = record
    FirstName: String;
    LastName: String;
  end;

  TDVDemoItemList = TList<TDVDemoItem>;

implementation

end.
