unit Main.Form;

interface

uses
  // Delphi Library Units
  System.Classes,
  System.SysUtils,
  System.Variants,
  Vcl.Controls,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.Forms,
  Vcl.Graphics,
  Vcl.StdCtrls,
  Winapi.Messages,
  Winapi.Windows;

type
  TfrmMain = class(TForm)
    btnOpenDataView: TButton;
    bvlLine: TBevel;
    btnClose: TButton;
    procedure btnOpenDataViewClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  // Project Units
  DVDemo.View.Form;

{$R *.dfm}

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.btnOpenDataViewClick(Sender: TObject);
var
  dataView: TfrmDVDemo;
begin
  dataView := TfrmDVDemo.Create(Self);
  dataView.Show;
end;

end.
