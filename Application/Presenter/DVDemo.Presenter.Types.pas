unit DVDemo.Presenter.Types;

interface

uses
  // Delphi Library Units
  System.Generics.Collections,
  // Project Units
  DVDemo.View.Interfaces;

type
  TDVDemoViewList = TList<IDVDemoView>;

implementation

end.
