unit DVDemo.Presenter.Interfaces;

interface

type
  IDVDemoPresenter = interface
    ['{52037538-8BB2-4153-8B1E-A962292C1796}']
  // public
    // Methods
    procedure OnAddItem;
    procedure OnChangeItem;
    procedure OnDeleteItem;
    procedure OnSelectItem;
  end;

implementation

end.
