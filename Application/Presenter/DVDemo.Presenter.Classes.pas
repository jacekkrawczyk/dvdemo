﻿unit DVDemo.Presenter.Classes;

interface

uses
  // Project Units
  DVDemo.Model.Interfaces,
  DVDemo.Presenter.Interfaces,
  DVDemo.Presenter.Types,
  DVDemo.View.Interfaces;

type

  { TDVDemoPresenter }

  TDVDemoPresenter = class(TInterfacedObject, IDVDemoPresenter)
  {$REGION 'Internal declarations'}
  strict private
    fView: IDVDemoView;
    class var fModel: IDVDemoModel; // nötig für neue Views um Daten zu laden...
    class var fViewList: TDVDemoViewList;
    // Methods
    procedure CheckIfModelItemIndexIsValid(const AItemIndex: Integer);
    procedure UpdateAllViews;
  {$ENDREGION}
  public
    constructor Create(const AView: IDVDemoView);
    destructor Destroy; override;
    // Methods
    procedure OnAddItem;
    procedure OnChangeItem;
    procedure OnDeleteItem;
    procedure OnSelectItem;
  end;

implementation

uses
  // Delphi Library Units
  System.Math,
  System.SysUtils,
  // Project Units
  DVDemo.Model.Classes,
  DVDemo.Types;

resourcestring
  strIndexDoesNotExistFmt = 'Index "%d" does not exist';

{ TDVDemoPresenter }

procedure TDVDemoPresenter.CheckIfModelItemIndexIsValid(
  const AItemIndex: Integer);
const
  INVALID_LIST_INDEX = -1;
begin
  if (AItemIndex = INVALID_LIST_INDEX) or (fModel.Count < AItemIndex) then
    raise EDVDemo.CreateFmt(strIndexDoesNotExistFmt, [AItemIndex]);
end;

constructor TDVDemoPresenter.Create(const AView: IDVDemoView);
begin
  fView := AView;

  if not Assigned(fModel) then
    fModel := TDVDemoModel.Create;

  if not Assigned(fViewList) then
    fViewList := TDVDemoViewList.Create;

  fViewList.Add(fView);
  UpdateAllViews;
end;

destructor TDVDemoPresenter.Destroy;
begin
  fViewList.Remove(fView);

  if IsZero(fViewList.Count) then
    FreeAndNil(fViewList);

  inherited;
end;

procedure TDVDemoPresenter.OnAddItem;
var
  item: TDVDemoItem;
begin
  Assert(Assigned(fView));
  Assert(Assigned(fModel));

  item.FirstName := fView.FirstNameUserInput;
  item.LastName := fView.LastNameUserInput;
  fModel.AddItem(item);

  UpdateAllViews;
end;

procedure TDVDemoPresenter.OnChangeItem;
var
  index: Integer;
  item: TDVDemoItem;
begin
  Assert(Assigned(fModel));
  Assert(Assigned(fView));

  index := fView.ListSelectedItemIndex;
  CheckIfModelItemIndexIsValid(index);

  item.FirstName := fView.FirstNameUserInput;
  item.LastName := fView.LastNameUserInput;
  fModel.Items[index] := item;

  UpdateAllViews;
end;

procedure TDVDemoPresenter.OnDeleteItem;
var
  index: Integer;
begin
  Assert(Assigned(fModel));
  Assert(Assigned(fView));

  index := fView.ListSelectedItemIndex;
  CheckIfModelItemIndexIsValid(index);

  fModel.RemoveItemFromIndex(index);

  UpdateAllViews;
end;

procedure TDVDemoPresenter.OnSelectItem;
var
  index: Integer;
  item: TDVDemoItem;
begin
  Assert(Assigned(fView));
  Assert(Assigned(fModel));

  index := fView.ListSelectedItemIndex;
  item := fModel.Items[index];
  fView.FirstNameUserInput := item.FirstName;
  fView.LastNameUserInput := item.LastName;
end;

procedure TDVDemoPresenter.UpdateAllViews;
var
  view: IDVDemoView;
begin
  Assert(Assigned(fModel));

  for view in fViewList do
    view.UpdateList(fModel.AsList);
end;

end.
