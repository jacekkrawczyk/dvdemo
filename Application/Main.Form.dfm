object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'DataView-Demo'
  ClientHeight = 120
  ClientWidth = 240
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    240
    120)
  PixelsPerInch = 96
  TextHeight = 13
  object bvlLine: TBevel
    Left = 8
    Top = 72
    Width = 224
    Height = 1
    Anchors = [akLeft, akRight, akBottom]
    Style = bsRaised
  end
  object btnOpenDataView: TButton
    Left = 8
    Top = 8
    Width = 224
    Height = 56
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Open a new DataView'
    TabOrder = 0
    OnClick = btnOpenDataViewClick
  end
  object btnClose: TButton
    Left = 112
    Top = 80
    Width = 120
    Height = 32
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 1
    OnClick = btnCloseClick
  end
end
