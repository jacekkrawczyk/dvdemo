unit DVDemo.Model.Interfaces;

interface

uses
  // Project Units
  DVDemo.Types;

type
  IDVDemoModel = interface
    ['{C26B2182-3BDD-40DF-9803-EB9172CA83BE}']
  // private
    // Property Accessors
    function GetAsList: TDVDemoItemList;
    function GetCount: Integer;
    function GetItems(const AIndex: Integer): TDVDemoItem;
    procedure SetItems(const AIndex: Integer; const AItem: TDVDemoItem);
  // public
    // Methods
    procedure AddItem(const AItem: TDVDemoItem);
    procedure RemoveItemFromIndex(const AIndex: Integer);
    // Properties
    property AsList: TDVDemoItemList read GetAsList;
    property Count: Integer read GetCount;
    property Items[const AIndex: Integer]: TDVDemoItem read GetItems
      write SetItems;
  end;

implementation

end.
