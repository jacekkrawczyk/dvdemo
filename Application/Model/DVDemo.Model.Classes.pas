unit DVDemo.Model.Classes;

interface

uses
  // Project Units
  DVDemo.Model.Interfaces,
  DVDemo.Types;

type

  { TDVDemoModel }

  TDVDemoModel = class(TInterfacedObject, IDVDemoModel)
  {$REGION 'Internal declarations'}
  strict private
    fList: TDVDemoItemList;
    // Property Accessors
    function GetAsList: TDVDemoItemList;
    function GetCount: Integer;
    function GetItems(const AIndex: Integer): TDVDemoItem;
    procedure SetItems(const AIndex: Integer; const AItem: TDVDemoItem);
    // Methods
    procedure CheckIfListIndexExists(const AListIndex: Integer);
  {$ENDREGION}
  public
    constructor Create;
    destructor Destroy; override;
    // Methods
    procedure AddItem(const AItem: TDVDemoItem);
    procedure RemoveItemFromIndex(const AIndex: Integer);
    // Properties
    property AsList: TDVDemoItemList read GetAsList;
    property Count: Integer read GetCount;
    property Items[const AIndex: Integer]: TDVDemoItem read GetItems
      write SetItems;
  end;

implementation

resourcestring
  strListItemDoesNotExistFmt = 'List index "%d" does not exist';

{ TDVDemoModel }

procedure TDVDemoModel.AddItem(const AItem: TDVDemoItem);
begin
  Assert(Assigned(fList));

  fList.Add(AItem);
end;

procedure TDVDemoModel.CheckIfListIndexExists(const AListIndex: Integer);
begin
  if fList.Count < AListIndex then
    raise EDVDemo.CreateFmt(strListItemDoesNotExistFmt, [AListIndex]);
end;

constructor TDVDemoModel.Create;
begin
  fList := TDVDemoItemList.Create;
end;

destructor TDVDemoModel.Destroy;
begin
  fList.Free;
  inherited;
end;

function TDVDemoModel.GetAsList: TDVDemoItemList;
begin
  Assert(Assigned(fList));

  Result := fList;
end;

function TDVDemoModel.GetCount: Integer;
begin
  Assert(Assigned(fList));

  Result := fList.Count;
end;

function TDVDemoModel.GetItems(const AIndex: Integer): TDVDemoItem;
begin
  Assert(Assigned(fList));

  CheckIfListIndexExists(AIndex);
  Result := fList.Items[AIndex];
end;

procedure TDVDemoModel.RemoveItemFromIndex(const AIndex: Integer);
begin
  Assert(Assigned(fList));

  CheckIfListIndexExists(AIndex);
  fList.Delete(AIndex);
end;

procedure TDVDemoModel.SetItems(const AIndex: Integer;
  const AItem: TDVDemoItem);
begin
  Assert(Assigned(fList));

  CheckIfListIndexExists(AIndex);
  fList.Items[AIndex] := AItem;
end;

end.
