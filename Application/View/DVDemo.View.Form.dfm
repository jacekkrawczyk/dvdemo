object frmDVDemo: TfrmDVDemo
  Left = 0
  Top = 0
  Caption = 'DataView'
  ClientHeight = 224
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    504
    224)
  PixelsPerInch = 96
  TextHeight = 13
  object bvlLine: TBevel
    Left = 8
    Top = 176
    Width = 489
    Height = 1
    Anchors = [akLeft, akRight, akBottom]
    Style = bsRaised
  end
  object grpList: TGroupBox
    Left = 8
    Top = 8
    Width = 240
    Height = 160
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'List'
    TabOrder = 0
    object lvList: TListView
      AlignWithMargins = True
      Left = 10
      Top = 19
      Width = 220
      Height = 131
      Margins.Left = 8
      Margins.Top = 4
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alClient
      Columns = <
        item
          Caption = 'First name'
          Width = 104
        end
        item
          AutoSize = True
          Caption = 'Last name'
        end>
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = lvListSelectItem
    end
  end
  object grpItem: TGroupBox
    Left = 256
    Top = 10
    Width = 240
    Height = 160
    Anchors = [akTop, akRight, akBottom]
    Caption = 'List item'
    TabOrder = 1
    object lblFirstName: TLabel
      Left = 8
      Top = 20
      Width = 50
      Height = 13
      Caption = 'First name'
    end
    object lblLastName: TLabel
      Left = 8
      Top = 52
      Width = 49
      Height = 13
      Caption = 'Last name'
    end
    object edtFirstName: TEdit
      Left = 112
      Top = 16
      Width = 120
      Height = 21
      TabOrder = 0
    end
    object edtLastName: TEdit
      Left = 112
      Top = 48
      Width = 120
      Height = 21
      TabOrder = 1
    end
    object btnChange: TButton
      Left = 8
      Top = 80
      Width = 104
      Height = 32
      Caption = 'Replace'
      TabOrder = 2
      OnClick = btnChangeClick
    end
    object btnAdd: TButton
      Left = 120
      Top = 80
      Width = 112
      Height = 32
      Caption = 'Add'
      TabOrder = 3
      OnClick = btnAddClick
    end
    object btnDelete: TButton
      Left = 8
      Top = 120
      Width = 224
      Height = 32
      Caption = 'Remove'
      TabOrder = 4
      OnClick = btnDeleteClick
    end
  end
  object btnClose: TButton
    Left = 376
    Top = 184
    Width = 120
    Height = 32
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 2
    OnClick = btnCloseClick
  end
end
