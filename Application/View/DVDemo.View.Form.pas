unit DVDemo.View.Form;

interface

uses
  // Delphi Library Units
  System.Classes,
  System.SysUtils,
  System.Variants,
  Vcl.ComCtrls,
  Vcl.Controls,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.Forms,
  Vcl.Graphics,
  Vcl.Grids,
  Vcl.StdCtrls,
  Vcl.ValEdit,
  Winapi.Messages,
  Winapi.Windows,
  // Project Units
  DVDemo.Presenter.Interfaces,
  DVDemo.Types,
  DVDemo.View.Interfaces;

type
  TfrmDVDemo = class(TForm, IDVDemoView)
    grpList: TGroupBox;
    grpItem: TGroupBox;
    bvlLine: TBevel;
    btnClose: TButton;
    lblFirstName: TLabel;
    edtFirstName: TEdit;
    lblLastName: TLabel;
    edtLastName: TEdit;
    btnChange: TButton;
    btnAdd: TButton;
    btnDelete: TButton;
    lvList: TListView;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure lvListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  {$REGION 'Internal declarations'}
  strict private
    fPresenter: IDVDemoPresenter;
    // Property Accessors
    function GetFirstNameUserInput: String;
    function GetLastNameUserInput: String;
    function GetListSelectedItemIndex: Integer;
    procedure SetFirstNameUserInput(const AFirstName: String);
    procedure SetLastNameUserInput(const ALastName: String);
  {$ENDREGION}
  public
    // Methods
    procedure UpdateList(const AList: TDVDemoItemList);
    // Properties
    property FirstNameUserInput: String read GetFirstNameUserInput
      write SetFirstNameUserInput;
    property LastNameUserInput: String read GetLastNameUserInput
      write SetLastNameUserInput;
    property ListSelectedItemIndex: Integer read GetListSelectedItemIndex;
  end;

var
  frmDVDemo: TfrmDVDemo;

implementation

uses
  // Project Units
  DVDemo.Presenter.Classes;

{$R *.dfm}

procedure TfrmDVDemo.btnAddClick(Sender: TObject);
begin
  Assert(Assigned(fPresenter));

  fPresenter.OnAddItem;
end;

procedure TfrmDVDemo.btnChangeClick(Sender: TObject);
begin
  Assert(Assigned(fPresenter));

  fPresenter.OnChangeItem;
end;

procedure TfrmDVDemo.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmDVDemo.btnDeleteClick(Sender: TObject);
begin
  Assert(Assigned(fPresenter));

  fPresenter.OnDeleteItem;
end;

procedure TfrmDVDemo.FormCreate(Sender: TObject);
begin
  fPresenter := TDVDemoPresenter.Create(Self);
end;

procedure TfrmDVDemo.FormDestroy(Sender: TObject);
begin
  // noop
end;

function TfrmDVDemo.GetFirstNameUserInput: String;
begin
  Result := edtFirstName.Text;
end;

function TfrmDVDemo.GetLastNameUserInput: String;
begin
  Result := edtLastName.Text;
end;

function TfrmDVDemo.GetListSelectedItemIndex: Integer;
begin
  Result := lvList.ItemIndex;
end;

procedure TfrmDVDemo.lvListSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if Selected then    // es ist zu GUI-speziphisch um es im Presenter zu beurteilen...
    fPresenter.OnSelectItem;
end;

procedure TfrmDVDemo.SetFirstNameUserInput(const AFirstName: String);
begin
  edtFirstName.Text := AFirstName;
end;

procedure TfrmDVDemo.SetLastNameUserInput(const ALastName: String);
begin
  edtLastName.Text := ALastName;
end;

procedure TfrmDVDemo.UpdateList(const AList: TDVDemoItemList);
var
  item: TDVDemoItem;
  listViewItem: TListItem;
begin
  lvList.Items.Clear;
  for item in AList do
  begin
    listViewItem := lvList.Items.Add;
    listViewItem.Caption := item.FirstName;
    listViewItem.SubItems.Add(item.LastName);
  end;
end;

end.
