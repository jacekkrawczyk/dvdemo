unit DVDemo.View.Interfaces;

interface

uses
  // Project Units
  DVDemo.Types;

type
  IDVDemoView = interface
    ['{620223F1-7DF9-4A76-AAA4-C799F830060D}']
  // private
    // Property Accessors
    function GetFirstNameUserInput: String;
    function GetLastNameUserInput: String;
    function GetListSelectedItemIndex: Integer;
    procedure SetFirstNameUserInput(const AFirstName: String);
    procedure SetLastNameUserInput(const ALastName: String);
  // public
    // Methods
    procedure UpdateList(const AList: TDVDemoItemList);
    // Properties
    property FirstNameUserInput: String read GetFirstNameUserInput
      write SetFirstNameUserInput;
    property LastNameUserInput: String read GetLastNameUserInput
      write SetLastNameUserInput;
    property ListSelectedItemIndex: Integer read GetListSelectedItemIndex;
  end;

implementation

end.
