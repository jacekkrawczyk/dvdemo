program DVDemo;

uses
  Vcl.Forms,
  Main.Form in 'Main.Form.pas' {frmMain},
  DVDemo.Presenter.Classes in 'Presenter\DVDemo.Presenter.Classes.pas',
  DVDemo.View.Interfaces in 'View\DVDemo.View.Interfaces.pas',
  DVDemo.Types in 'DVDemo.Types.pas',
  DVDemo.Presenter.Interfaces in 'Presenter\DVDemo.Presenter.Interfaces.pas',
  DVDemo.Model.Interfaces in 'Model\DVDemo.Model.Interfaces.pas',
  DVDemo.Model.Classes in 'Model\DVDemo.Model.Classes.pas',
  DVDemo.View.Form in 'View\DVDemo.View.Form.pas' {frmDVDemo},
  DVDemo.Presenter.Types in 'Presenter\DVDemo.Presenter.Types.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'DataView Demo';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmDVDemo, frmDVDemo);
  Application.Run;
end.
